Tor Project Board Meeting
December 04, 2023, 17:00 UTC - 18:30 UTC

Board In Attendance:
--------------------

* Esra'a Al Shafei
* Kendra Albert
* Dees Chinniah
* Alissa Cooper
* Sarah Gran
* Christian Kaufmann
* Julius Mittenzwei

Non-Board Attendees:
--------------------

* Susan Abt, CFO
* Lisa Rodriguez, AMS
* Pavel Zoneff, Director of Communications

Quorum was achieved. The meeting was called to order at 17:05 UTC.

Term Renewals
-------------

Sarah Gran, Esra’a Al Shafei, and Christian Kaufmann have completed their first year of board service, which triggers a need for the board to vote to renew their terms. Kendra reminded the Board that per the bylaws the term renewals will be for a 2-year term.

Sarah Gran 
Sarah left for this portion of the meeting. Kendra asked for any comments or questions. 

Alissa motioned to elect Sarah for another 2-year term. Kendra seconded.
A ballot vote was held via direct message to chat. Kendra confirmed that all votes were received. 
All voted in favor. There were no objections or abstentions.

Sarah’s term will end December 15, 2025.

Esra'a Al Shafei
Esra’a left for this portion of the meeting. Kendra asked for any comments or questions. 

Kendra motioned to elect Esra’a for another 2-year term. CK seconded.
A ballot vote was held via direct message to chat. Kendra confirmed that all votes were received. 
All voted in favor. There were no objections or abstentions.

Esra’a’s term will end December 15, 2025.

Christian Kaufmann
CK left for this portion of the meeting. Kendra asked for any comments or questions. 

Kendra motioned to elect CK for another 2-year term. Alissa seconded.
A ballot vote was held via direct message to chat. Kendra confirmed that all votes were received. 
All voted in favor. There were no objections or abstentions.

CK’s term will end December 15, 2025.

Kendra took a moment to note Biella Coleman’s resignation from the board via email. It was clarified that Alissa is the current clerk and this resignation will not affect that officer position. 

Kendra said that they have not yet discussed with Isa how to move forward regarding Biella’s vacant board seat. Should there be an open call or is the Board hoping to fill a specific need or goal? Kendra will discuss with Isa before providing the Board with more information so the Board can decide how best to move forward. 

Kendra opened the floor for any thoughts or questions.
Julius noted that his term will end in a year. It might be more efficient to wait and search for 2 Board members. Kendra agreed and said that Board terms will be taken into consideration.
There was general agreement that having an odd number of Board members is not crucial but might be considered.

Board Meeting Scheduling Discussion
-----------------------------------

The current Board schedule is once a quarter for two hours. The Q1 meeting has been for yearly planning, Q2 for budget, Q3 for elections and Q4 for audit and financials.

Kendra suggested adjusting the schedule to 5 shorter meetings (closer to an hour and a half) plus the in-person meeting. This will give more flexibility to schedule meetings based on what makes sense for the Board at the time. They asked the Board for their thoughts.

There was general agreement that 5 meetings would work and that the length of the meetings do not need to be set. It was noted that there is a preference for meetings to be scheduled further in advance.  

Since there were no objections to the additional meetings, Kendra will send a more detailed scheduling proposal to the Board.  

Opportunities for Board Connection
----------------------------------

CK suggested that the Board create a Signal chat in order to improve communication. He said that it might be good for quick updates and to create a stronger connection.

Kendra said that the Board used to meet in person twice a year pre-pandemic. They discussed the possibility of a Board retreat possibly in conjunction with a conference. They asked if it is something the Board might be interested in. 

In chat and in the meeting, Board members expressed their agreement for both the Signal chat and another in-person meeting. The Signal chat group will be created. There will be more discussion about an additional meeting.

Investment Policy Updates and Discussion
----------------------------------------

Sue gave an overview of the Investment Policy Statement. There are a couple of remaining comments that she is hoping can be addressed so the document can be approved. The next step will be to go to investment firms and request a proposal. 

The board reviewed Alissa’s comments. Her initial comment about the document title was resolved. 

Alissa’s second comment under Procedures in section 2D was discussed by the Board. 

Sue explained that her general rule of thumb is to have 6 times the monthly budget available in the operating fund. She will look for further guidance from an investment manager but would like to approach investment firms with a Board approved policy. 

After some discussion, the verbiage was updated to clarify that any changes to the amounts allocated to the different funds would be brought to the Board for approval. If there are any strong suggested changes to the policy by the chosen investment firm, the Board can revisit and make updates.

Kendra asked for any further comments or questions. After all were addressed, Kendra motioned to approve the Investment Policy Statement. CK seconded.

All voted in favor. There was one abstention.

Sue will keep the Board updated on the process.

ATOR Update
-----------

Pavel gave an update on what ATOR is. He gave a timeline of the group. 
He reviewed the trademark violations and the influx of ATOR relays.

There has been a commitment by ATOR to do a complete rebrand by February of next year. It is unclear if ATOR will meet this deadline. 

Pavel said that they are continuing to remind ATOR of their commitment to rebrand by February. On a positive note, this has moved along and improved Tor trademark work, brand, and network health policies. 

Pavel will continue to keep the Board updated.

-------------

The open session closed at 18:15 UTC.

●	Executive Session
	o	Update on ED Feedback Process

