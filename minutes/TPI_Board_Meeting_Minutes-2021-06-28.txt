Tor Board Meeting Minutes for June 28, 2021

Video and phone conference meeting called to order at 12:00 EST/ 16 UTC 

Present: Isabela Bagueros, Alissa Cooper, Desigan Chinniah, Kendra Albert, Nick Mathewson, Sue Abt, Biella Coleman, Cindy Cohn, Roger Dingledine, Rabbi Rob, Julius Mittenzwei Chelsea Kolmo, Ramy Raoof, Nighat Dad, Silvia Puglisi.



I. Board Onboarding

General Expectations of Boards � Cindy
o Hire/Fire ED
o Support Isabela
Tor Board Specific Expectations � Rabbi Rob
o Vigorous engagement model
o Linkages between communities, cross pollination; force multiplayer. Plug into communities that best fit you.
o Committees
o Mentorship
Tor Board Schedule -- Biella
o Will schedule 8-10 weeks
o Will have a time that might work for everyone but then 8-10 weeks do a 
o Generally 15/16 UTC start time that works, historically
 Tor organization, bylaws and other documents-- Sue, Isabela
o Sue, Grants Management, Internal Control Overview
o Conflict of Interest Policy, Needs to be filled out at the beginning of the fiscal year and please send to Sue. 
o Directors and Officer Policy�Just renewed (insurance)
o Structure of Tor, Isabela
o Working map graph (Isa will send the graph and pdf later)/ Structure of Tor.

Committee�s Overview � Chelsea
o Introducing committees to the core board
o 3 - 5 per month commitment between committees and Tor board meetings
o Go deeper into some piece of TOR
o Finance, Marketing and Legal
o There will be clerk/chair. 
o It�s good to meet before next meeting
Break

Committee�s Overview (details of general expectation are on the board book)
o Isabela and Sue, Finance Overview
o Kendra: Legal overview
o Email lists for each committee

Part Two of Meeting

1. Administrivia

 
 Announcement of Cindy Cohn�s resignation
Approval of January 15, 2021 minutes: Biella moved the motion, Kendra seconded, all voted in favor.

Cindy Cohn left the meeting shortly after 1 pm est

2. Financial Updates

June ending fiscal year.  Ending in Surplus (over 2 million)
Profit and Losses, Sue and Isabela
o New comparison across various times period/chart
Sue, changes made based on auditor suggestions (overview)
o Good position, esp compared to last year.
o Sue now has month to month budget
	Isabela will send the full narrative for July 1 2021-June 2022 budget
Isabela presented waterfall (2020 vision vs May 31. 2021, updated)
o Dees raised whether NFT will be a yearly event, likely not. Sue suggested to separate out NFT as its own category (for clarity around budget numbers)
Isabela presented  a breakdown of the fundraising projects (e.g.,  Individual Gifts, Major Donors, Monthly Donors, Large foundations, NFT, Defcon, etc.)
Isabela presented the NFT overview
Isabel presented pipeline report, grant overview (what is available now, next year, and beyond next year)
Isabela presented grants applied (TBD, e.g. Sloan, DRL) and grants awarded and grants rejected.
o Chelsea recommended we do something similar with corporations using a similar breakdown.
Isabela Membership Program, Overview
o Renewals are now starting 
o https://www.torproject.org/about/membership/
Dees asked about restricted vs unrestricted funds + overview
o 600,000 to 900,000 in unrestricted (we�ve grown)
o Most everything from grants is restricted
o Membership program unrestricted
o Some foundation grants are unrestricted

3. Salary Raise

Everyone has been given a 5% rise (unless moved position or new hires, as these position or hires already had the raise built in).
Board needs to official approve the raise for the ED Isabela Bagueros
Rabbi Rob  move to increase Isabela�s salary by 5% effective July 1 2021, Julius seconded.  All voted in favor

RESOLVED: Tor Board voted and approved a 5% raise for the Tor, Inc., ED Isabela Bagueros




Meeting adjourned at 18:00 UTC