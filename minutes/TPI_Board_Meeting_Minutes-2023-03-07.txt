Tor Project Board Meeting Minutes
March 7, 2023, 14:00 UTC - 16:00 UTC

Board In Attendance:
* Esra'a Al Shafei
* Kendra Albert
* Desigan Chinniah
* Biella Coleman
* Alissa Cooper
* Sarah Gran
* Christian Kaufmann
* Julius Mittenzwei

Non-Board Attendees:
* Sue Abt, CFO
* Isabela Fernandes, ED
* Roger Dingeldine, Co-Founder
* Nick Mathewson, Co-Founder and Network Dev
* Lisa Rodriguez, AMS
* George Rosamund, Board Liaison

Quorum was achieved. The meeting was called to order at 14:00 UTC.

I.  Welcome/intros for new board members


II. Update on developing legal situation


III. Isa provided an overview with slides of the 2023 Strategic Plan

§  The plan is the fifth and final year of a 5 year plan, though some initiatives will continue to roll over.
§  Isabela will present this overview to Tor during an all hands meeting (and will share the details/slides)
§  We discussed how to track progress for some of these goals with measurements, noting that not all of them lend themselves to such quantification, which will be added to the ED review and assessment process

Closed Session
IV. Discussion of ED evaluation process


Meeting adjourned at 11 am/16 utc.

--

[Meeting minutes have been approved via email by all board members on 2023-03-26.]

